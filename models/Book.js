var mongoose = require('mongoose');

var BookSchema = new mongoose.Schema({
  id: String,
  title: String,
  description: String
});

module.exports = mongoose.model('Book', BookSchema);