function getData () {
  const title = document.getElementById("title").value.toString();
  const description = document.getElementById("description").value;
  //const artist = document.getElementById("artist").value;

  console.log('title ',typeof title);
  console.log('lyrics ',typeof description);
 // console.log('artist ', artist);

  const data = `
    title: "${title}", 
    description: "${description}"
  `
  const mutation = `
  mutation {
    addBook(${data}) {
      _id
    }
  }
`;

  var url = "/graphql";
  var opts = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ query: mutation })
  };

  fetch(url, opts)
    .then(res => res.json())
    .then(console.log)
    .catch(console.error);
}

function fetchData() {
  console.log('Fetch data');

  fetch('/graphql', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    body: JSON.stringify({query: "{ books { title } }"})
  })
    .then(r => r.json())
    .then(data => console.log('data returned:', data));
}


document.addEventListener('DOMContentLoaded', () => {
  let fetchButton = document.getElementById('fetch-data');
  let submitButton = document.getElementById('submit');

  submitButton.addEventListener('click', (e) => {
    e.preventDefault();
    getData();
  });

  fetchButton.addEventListener('click', (e) => {
    e.preventDefault();
    fetchData();
  });
});