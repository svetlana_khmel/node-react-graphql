This is a test project to test qraphQl queries with mongodb and Express.
Tools in use:
- express,
- express-graphql,
- jade,
- mongoose.

### Articles: 
### Node, Express, React.js, Graphql and MongoDB CRUD Web Application
https://www.djamware.com/post/5cbd1e9a80aca754f7a9d1f2/node-express-reactjs-graphql-and-mongodb-crud-web-application

Fetching Data from a GraphQL API:
https://moonhighway.com/fetching-data-from-a-graphql-api

4 simple ways to call a GraphQL API
https://blog.apollographql.com/4-simple-ways-to-call-a-graphql-api-a6807bcdb355?gi=1439231cba20

CheatSheet:
https://wehavefaces.net/graphql-shorthand-notation-cheatsheet-17cd715861b6#.9oztv0a7n

### How to test:
http://localhost:3000/graphql

````
{
books {
    _id
    title
  }
}


To get a single book by ID, use this GraphQL query.

{
  book(id: "5c738dd4cb720f79497de85c") {
    _id
        title
        description
  }
}


To add a book, use this GraphQL mutation.

 mutation {
   addBook(
     title: "Whatever this Book Title",
     description: "The short explanation of this Book",
   ) {
     _id
   }
 }
You will the response at the right pane like this.

{
  "data": {
    "addBook": {
      "updated_date": "2019-02-26T13:55:39.160Z"
    }
  }
}
To update a book, use this GraphQL mutation.

mutation {
  updateBook(
    id: "5c75455b146dbc2504b94012",
    isbn: "12345678221",
    title: "The Learning Curve of GraphQL",
    author: "Didin J.",
    description: "The short explanation of this Book",
    publisher: "Djamware Press",
    published_year: 2019
  ) {
    _id,
    updated_date
  }
}
You will see the response in the right pane like this.

{
  "data": {
    "updateBook": {
      "_id": "5c75455b146dbc2504b94012",
        title: "The Learning Curve of GraphQL",
        author: "Didin J.",
        description: "The short explanation of this Book",
    }
  }
}
To delete a book by ID, use this GraphQL mutation.

mutation {
  removeBook(id: "5c75455b146dbc2504b94012") {
    _id
  }
}
You will see the response in the right pane like this.

{
  "data": {
    "removeBook": {
      "_id": "5c75455b146dbc2504b94012"
    }
  }
  
  
  
  
  const { createApolloFetch } = require('apollo-fetch');
  
  const fetch = createApolloFetch({
    uri: 'https://1jzxrj179.lp.gql.zone/graphql',
  });
  
  fetch({
    query: '{ posts { title }}',
  }).then(res => {
    console.log(res.data);
  });
  
  // You can also easily pass variables for dynamic arguments
  fetch({
    query: `query PostsForAuthor($id: Int!) {
      author(id: $id) {
        firstName
        posts {
          title
          votes
        }
      }
    }`,
    variables: { id: 1 },
  }).then(res => {
    console.log(res.data);
  });
}
``